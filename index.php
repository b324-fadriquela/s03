<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<title>S03: Classes and Objects</title>
	</head>
	<body>
		<h1>Person</h1>
		<p><?php echo $basicPerson->Interact(); ?></p>

		<h1>Developer</h1>
		<p><?php echo $developer->Interact(); ?></p>

		<h1>Engineer</h1>
		<p><?php echo $engineer->Interact(); ?></p>
	</body>
</html>