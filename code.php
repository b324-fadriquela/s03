<?php 




class Person{
	public $fName;
	public $mName;
	public $lName;


	public function __construct($fName, $mName, $lName, ){

		$this->fName = $fName;
		$this->mName = $mName;
		$this->lName = $lName;

	}

	public function Interact(){
		echo "Your full name is  $this->fName  $this->mName  $this->lName.";
	}

}



class Developer extends Person{
	public function Interact(){
		echo "Your name is  $this->fName  $this->mName  $this->lName and you are a developer.";
	}
}

class Engineer extends Person{
	public function Interact(){
		echo "Your are an engineer named  $this->fName  $this->mName  $this->lName.";
	}
}


$basicPerson = new Person('Young','Jay','Welt');
$developer = new Developer('Larry', 'Von','Bradford');
$engineer = new Engineer('Timmy', 'Ashbury', 'Ashton');


 ?>